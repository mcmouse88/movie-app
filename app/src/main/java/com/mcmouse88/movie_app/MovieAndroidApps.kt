package com.mcmouse88.movie_app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MovieAndroidApps : Application() {
}