package com.mcmouse88.movie_app

import com.mcmouse88.movie_app.data.network.ApiService
import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getAllMovies() = apiService.getAllMovies()
}