package com.mcmouse88.movie_app.data.model

data class Country(
    val code: String,
    val name: String,
    val timezone: String
)