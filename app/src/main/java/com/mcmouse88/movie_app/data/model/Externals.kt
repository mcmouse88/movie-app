package com.mcmouse88.movie_app.data.model

data class Externals(
    val imdb: String,
    val thetvdb: Int,
    val tvrage: Int
)