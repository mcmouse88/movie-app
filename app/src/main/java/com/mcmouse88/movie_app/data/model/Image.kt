package com.mcmouse88.movie_app.data.model

data class Image(
    val medium: String,
    val original: String
)