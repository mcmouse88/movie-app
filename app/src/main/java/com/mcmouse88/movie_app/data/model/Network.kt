package com.mcmouse88.movie_app.data.model

data class Network(
    val country: Country,
    val id: Int,
    val name: String,
    val officialSite: String
)