package com.mcmouse88.movie_app.data.model

data class Schedule(
    val days: List<String>,
    val time: String
)