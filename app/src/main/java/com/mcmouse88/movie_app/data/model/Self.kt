package com.mcmouse88.movie_app.data.model

data class Self(
    val href: String
)