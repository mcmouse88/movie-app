package com.mcmouse88.movie_app.data.network

import com.mcmouse88.movie_app.data.model.Movies
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("shows")
    suspend fun getAllMovies() : Response<List<Movies>>
}