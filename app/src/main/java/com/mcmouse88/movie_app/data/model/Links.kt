package com.mcmouse88.movie_app.data.model

data class Links(
    val previousepisode: Previousepisode,
    val self: Self
)