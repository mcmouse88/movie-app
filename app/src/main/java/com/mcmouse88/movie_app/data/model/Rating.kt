package com.mcmouse88.movie_app.data.model

data class Rating(
    val average: Double
)