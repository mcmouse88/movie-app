package com.mcmouse88.movie_app

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.compose.rememberNavController
import com.mcmouse88.movie_app.navigation.SetupNavHost
import com.mcmouse88.movie_app.ui.theme.MovieAppTheme
import dagger.hilt.android.AndroidEntryPoint

// Чтобы приложение работало нужно в манифесте прописать
// android:name="Название класса помеченного аннотацией @HiltAndroidApp"
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MovieAppTheme {
                // Создадим переменную типа NavHostController
                val navController = rememberNavController()
                val mainViewModel = hiltViewModel<MainViewModel>()
                // Добавим SetupNavHost
                SetupNavHost(navController = navController, viewModel = mainViewModel)
            }
        }
    }
}