package com.mcmouse88.movie_app.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.mcmouse88.movie_app.MainViewModel
import com.mcmouse88.movie_app.utils.ParsingHTMLText

@OptIn(ExperimentalCoilApi::class)
@Composable
fun DetailScreen(
    viewModel: MainViewModel,
    itemId: String
) {
    val currentItem = viewModel.allMoviesList
        .observeAsState(listOf()).value
        .firstOrNull { it.id == itemId.toInt() }
    Surface(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 24.dp, horizontal = 8.dp)
    ) {
        LazyColumn(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            item {
                Image(
                    painter = rememberImagePainter(currentItem?.image?.original),
                    contentDescription = null,
                    modifier = Modifier.size(420.dp)
                )
                Text(
                    text = currentItem?.name ?: "",
                    fontWeight = FontWeight.Bold,
                    fontSize = 32.sp,
                    modifier = Modifier.padding(top = 16.dp)
                )

                Row(
                    modifier = Modifier.padding(8.dp)
                ) {
                    Text(
                        text = "Rating: ",
                        fontWeight = FontWeight.Bold,
                        fontSize = 18.sp
                    )

                    Text(
                        text = currentItem?.rating?.average.toString(),
                        fontSize = 18.sp
                    )
                }
                Row(
                    modifier = Modifier.padding(8.dp)
                ) {
                    Text(
                        text = "Genre: ",
                        fontWeight = FontWeight.Bold,
                        fontSize = 18.sp
                    )

                    currentItem?.genres?.take(2)?.forEach {
                        Text(
                            text = " $it ",
                            fontSize = 18.sp
                        )

                    }
                }
                ParsingHTMLText(
                    text = currentItem?.summary ?: "",
                    modifier = Modifier.padding(top = 10.dp)
                )
            }
        }
    }
}